# frozen_string_literal: true

module Chemlab
  # Element types
  module Element
    # Elements that can be clicked
    CLICKABLES = %i[
      a
      button
      link
      checkbox
      image
      radio
    ].freeze

    # Elements that are +<select>+
    SELECTABLES = %i[
      select
    ].freeze

    # Text input elements
    INPUTS = %i[
      text_field
      text_area
    ].freeze
  end
end
