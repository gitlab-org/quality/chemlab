# frozen_string_literal: true

module Chemlab
  module Runtime
    # Environment configuration file
    module Env
      class << self
        # @return [Boolean] true if debug mode is enabled
        # @example
        #   ENV['CHEMLAB_DEBUG'] = 'true'
        #   Runtime::Env.debug? #=> true
        def debug?
          enabled?(ENV['CHEMLAB_DEBUG'], default: false)
        end

        private

        def enabled?(value, default: true)
          return default if value.nil?

          (value =~ /^(false|no|n|0)$/i) != 0
        end
      end
    end
  end
end
