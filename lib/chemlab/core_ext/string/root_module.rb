# frozen_string_literal: true

# Start monkey patch of String
class String
  # Find the root module (parent module) of a class or module
  # @example
  #   'A::B::C'.root_module #=> A
  #   'A::B'.root_module => A
  # @return [Module] the root module
  def root_module
    Object.const_get(split('::').first)
  end
end
