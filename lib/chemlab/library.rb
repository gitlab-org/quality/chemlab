# frozen_string_literal: true

module Chemlab
  # Application Library Definition
  # Provides accessors for +:base_url+, +:base_url=+
  # +base_url+ will default to Chemlab's configured base_url
  module Library
    def self.included(base)
      base.module_eval do
        class << self
          # The Base URL where this library / application exists
          # If you have multiple applications that Chemlab will target,
          # this is useful for having separate base_urls.
          #
          # @return [String] the base_url.  +Chemlab.configuration.base_url+ by default if not set
          #
          # @example
          #   Chemlab.configure do |chemlab|
          #     A.base_url = 'https://first_app'
          #     B.base_url = 'https://second_app'
          #     chemlab.base_url = 'https://main_app'
          #
          #     chemlab.libraries = [A, B, C]
          #
          #     C.base_url #=> https://main_app
          #   end
          attr_writer :base_url

          def base_url
            @base_url ||= Chemlab.configuration.base_url
          end
        end
      end
    end
  end
end
