# frozen_string_literal: true

require 'erb'

module Chemlab
  module CLI
    # Base Chemlab class generator ($ chemlab generate ...)
    module Generator
      extend self

      INVALID_GENERATOR_ERROR = <<~ERR
        Cannot generate `%s` as the generator does not exist.
        Possible options are %s.
      ERR

      def generate(what, name, args)
        raise ArgumentError, 'Please specify what to generate' unless what
        raise ArgumentError, 'Please specify a name' unless name

        unless possible_generators.has_key?(what)
          raise ArgumentError, INVALID_GENERATOR_ERROR % what,
                possible_generators.keys.join(',')
        end

        data = args.each_with_object({}) do |arg, h|
          k, v = arg.split('=')
          h[k] = v
        end

        # render the erb
        $stdout.puts ERB.new(File.read(possible_generators[what]),
                             trim_mode: '%<>').result_with_hash({ data: data, name: name })
      end

      private

      # List the possible generators
      # @return [Hash] { 'generator' => 'templates/generator.erb' }
      def possible_generators
        Dir[File.expand_path('./generator/templates/*.erb', __dir__)].each_with_object({}) do |generator, generators|
          generators[File.basename(generator)[0..-5]] = generator
        end
      end
    end
  end
end
