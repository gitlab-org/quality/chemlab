# frozen_string_literal: true

namespace :generate do
  desc <<-DESCRIPTION
    Generate page library stubs

    Arguments: PATH
    Examples:
      - generate:stubs qa/page/login.rb
        Generate stubs for a specific page library
      - generate:stubs qa/page
        Generates stubs for all page libraries in `qa/page`
  DESCRIPTION
  task :stubs do |_, path|
    puts "Doin some stubbin in #{path}"

    require_relative '../chemlab/cli/stubber'
    Chemlab::CLI::Stubber.stub_all(path)
  end
end
