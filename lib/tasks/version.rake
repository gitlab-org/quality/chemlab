# frozen_string_literal: true

desc 'Shows the version'
task '--version' do
  require_relative '../chemlab/version'

  $stdout.puts %(#{$PROGRAM_NAME} version #{Chemlab::VERSION})
end
