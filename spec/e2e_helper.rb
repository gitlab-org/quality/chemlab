# frozen_string_literal: true

require 'chemlab'

module ExampleApp
  include Chemlab::Library

  class Index < Chemlab::Page
    path '/'

    h1 :header, css: 'h1'
  end
end

module Gitlab
  include Chemlab::Library

  class SignIn < Chemlab::Page
    path '/users/sign_in'

    text_field :username, id: 'user_login'
  end
end

Chemlab.configure do |chemlab|
  chemlab.browser = :chrome

  ExampleApp.base_url = 'https://example.com'
  chemlab.base_url = 'https://gitlab.com'

  chemlab.libraries = [ExampleApp, Gitlab]
end
