# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  RSpec.describe Attributable do
    subject(:clazz) do
      Class.new do
        include Attributable

        attribute :id
        attribute :name do
          'test'
        end
      end.new
    end

    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:name) }

    it 'defaults to nil' do
      expect(clazz.id).to be_nil
    end

    it 'allows a value' do
      expect(clazz.name).to eq('test')
    end
  end
end
