# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Chemlab do
  let(:base_url) { 'test' }

  describe '.configuration' do
    subject do
      described_class.configuration do |config|
        config.hide_banner = true
      end
    end

    it { is_expected.to be_a(Chemlab::Configuration) }
  end
end
