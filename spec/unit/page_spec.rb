# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  RSpec.describe Page do
    subject(:page_instance) do
      page.new
    end

    let(:page) do
      Class.new(described_class) do
        path '/login'

        text_field :username
        text_field :password
      end
    end

    describe '#path' do
      it 'returns the path' do
        expect(page.path).to eq('/login')
      end

      it 'raises an error if the path of the page is not preceded with a slash'
    end

    describe '#visit' do
      let(:watir) { instance_spy('watir') }
      let(:base_url) { 'https://example.com' }

      before do
        stub_const('Watir::Browser', watir)

        Chemlab.configure do |chemlab|
          chemlab.browser = :chrome
          chemlab.base_url = base_url
          chemlab.hide_banner = true
        end

        page_instance.visit
      end

      it 'calls Watir::Browser#goto' do
        expect(watir).to have_received(:goto).with("#{base_url}#{page.path}")
      end
    end

    describe '#visible?' do
      let(:component) { instance_spy('component') }

      let(:page_with_elements) do
        Class.new(described_class) do
          div :test
        end.new
      end

      let(:page_with_optional_elements) do
        Class.new(described_class) do
          div :test, :optional
        end.new
      end

      context 'when on page and elements are there' do
        before do
          allow(page_with_elements).to receive(:has_element?).and_return(true)
        end

        it 'returns true when everything is ok' do
          expect(page_with_elements).to be_visible
        end
      end

      context 'when not on page' do
        before do
          allow(page_with_elements).to receive(:has_element?).and_return(false)
        end

        it 'returns false when no element is there' do
          expect(page_with_elements).not_to be_visible
        end
      end

      context 'when on page but elements are missing' do
        it 'works'
      end
    end

    describe '#on_page?' do
      context 'when on right page' do
        before do
          allow(Chemlab.configuration).to receive(:browser).and_return(
            Struct.new(:url).new("https://example.com#{page.path}")
          )
        end

        it 'returns true' do
          expect(page_instance).to be_on_page
        end
      end

      context 'when on different page' do
        before do
          allow(Chemlab.configuration).to receive(:browser).and_return(
            Struct.new(:url).new('https://example.com/different/page')
          )
        end

        it 'returns false' do
          expect(page_instance).not_to be_on_page
        end
      end
    end

    describe 'page elements' do
      it { is_expected.to respond_to(:username) }
      it { is_expected.to respond_to(:username=) }
      it { is_expected.to respond_to(:username?) }
      it { is_expected.to respond_to(:username_element) }
      it { is_expected.to respond_to(:password) }
      it { is_expected.to respond_to(:password=) }
      it { is_expected.to respond_to(:password?) }
      it { is_expected.to respond_to(:password_element) }
    end
  end
end
