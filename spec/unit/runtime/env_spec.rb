# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  module Runtime
    RSpec.describe Env do
      describe '#debug?' do
        using RSpec::Parameterized::TableSyntax

        where(:value, :debug?) do
          'yes' | true
          'y'   | true
          'YES' | true
          'Yes' | true
          'Y'   | true
          '1'   | true
          'whatever' | true

          'no' | false
          'n'  | false
          'NO' | false
          'No' | false
          'N'  | false
          '0'  | false
        end

        with_them do
          it 'is enabled or disabled' do
            ClimateControl.modify(CHEMLAB_DEBUG: value) do
              expect(described_class.debug?).to eq(debug?)
            end
          end
        end
      end
    end
  end
end
