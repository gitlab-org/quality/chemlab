# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  module Runtime
    RSpec.describe Browser do
      subject(:browser) do
        described_class.new(options)
      end

      let(:watir) { instance_spy('watir') }
      let(:options) { [:chrome, { headless: true }] }

      before do
        stub_const('Watir::Browser', watir)
      end

      describe '#new' do
        it 'starts a new session' do
          browser

          expect(watir).to have_received(:new).with(:chrome, headless: true)
        end
      end

      describe 'delegators' do
        it { is_expected.to respond_to(:url) }
        it { is_expected.to respond_to(:refresh) }
        it { is_expected.to respond_to(:text) }
        it { is_expected.to respond_to(:quit) }
      end

      describe '#navigate_to' do
        let(:watir) { instance_spy('watir') }
        let(:base_url) { 'https://example.com' }
        let(:path) { '/path' }

        before do
          stub_const('Watir::Browser', watir)

          Chemlab.configure do |chemlab|
            chemlab.browser = :chrome
            chemlab.base_url = base_url
            chemlab.hide_banner = true
          end

          browser.navigate_to(path)
        end

        it 'calls Watir::Browser#goto' do
          expect(watir).to have_received(:goto).with(path)
        end
      end

      describe '.navigate_to' do
        let(:watir) { instance_spy('watir') }
        let(:base_url) { 'https://test.example.com' }

        let!(:_) do
          module ::Lib
            include Chemlab::Library

            class Page < Chemlab::Page
              path '/test'
            end
          end
        end

        before do
          stub_const('Watir::Browser', watir)

          Chemlab.configure do |chemlab|
            chemlab.browser = :chrome
            chemlab.base_url = 'https://example.com'
            ::Lib.base_url = base_url
            chemlab.hide_banner = true
          end

          described_class.navigate_to(::Lib::Page)
        end

        it 'navigates to a libraries base_url' do
          expect(watir).to have_received(:goto).with("#{base_url}/test")
        end
      end

      describe '#to_s' do
        it 'returns the browser options' do
          expect(browser.to_s).to eq(options.to_s)
        end
      end
    end

    RSpec.describe Browser::Session do
      subject(:session) do
        described_class.new(options)
      end

      let(:watir) { instance_spy('watir') }
      let(:options) { [:chrome, { headless: true }] }


      before do
        stub_const('Watir::Browser', watir)
      end

      describe 'delegators' do
        it { is_expected.to respond_to(:url) }
        it { is_expected.to respond_to(:refresh) }
        it { is_expected.to respond_to(:text) }
        it { is_expected.to respond_to(:quit) }
      end
    end
  end
end
