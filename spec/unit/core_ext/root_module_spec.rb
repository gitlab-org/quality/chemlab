# frozen_string_literal: true

require 'spec_helper'
require 'chemlab/core_ext/string/root_module'

module A; end

RSpec.describe String do
  shared_examples 'root module' do |string, mod: A|
    it 'returns the root module' do
      expect(string.root_module).to eq(mod)
    end
  end

  describe '#root_module' do
    it_behaves_like 'root module', 'A'
    it_behaves_like 'root module', 'A::B'
    it_behaves_like 'root module', 'A::B::C'
  end
end
