# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  RSpec.describe Component do
    subject(:component_instance) { component.new }

    let(:component) do
      Class.new(described_class) do
        div :test_div
        text_field :test_text_field
        button :test_button
      end
    end

    let(:component_with_custom_args) do
      Class.new(described_class) do
        div :test_div, id: 'test', wait: 20
        text_field :test_text_field, id: 'field', wait_until: %i[exists? enabled?]
        button :test_button, id: 'button' # buttons are defaulted to wait for clickable?
      end
    end

    describe 'elements' do
      it { is_expected.to respond_to(:test_div) }
      it { is_expected.to respond_to(:test_div?) }
      it { is_expected.to respond_to(:test_div_element) }
      it { is_expected.not_to respond_to(:test_div=) }

      it { is_expected.to respond_to(:test_text_field) }
      it { is_expected.to respond_to(:test_text_field=) }
      it { is_expected.to respond_to(:test_text_field?) }
      it { is_expected.to respond_to(:test_text_field_element) }

      it { is_expected.to respond_to(:test_button) }
      it { is_expected.to respond_to(:test_button?) }
      it { is_expected.to respond_to(:test_button_element) }
      it { is_expected.not_to respond_to(:test_button=) }
    end

    describe '#public_elements' do
      it 'is well-formed' do
        expect(component.public_elements).to match_array([
          { type: :div, name: :test_div, args: [] },
          { type: :text_field, name: :test_text_field, args: [] },
          { type: :button, name: :test_button, args: [] }
        ])
      end
    end

    describe 'waiting' do
      let(:browser) { instance_spy('browser') }

      before do
        stub_const('Watir::Browser', browser)

        Chemlab.configure do |chemlab|
          chemlab.browser = :chrome
          chemlab.hide_banner = true
        end
      end

      it 'Chemlab.configuration.default_timeout defaults to Watir.default_timeout' do
        expect(Chemlab.configuration.default_timeout).to eq(Watir.default_timeout)
      end

      it 'changing Chemlab.configuration.default_timeout changes Watir.default_timeout' do
        Chemlab.configuration.default_timeout = 10
        expect(Watir.default_timeout).to eq(10)
      end

      context 'when finding an element' do
        let(:watir_element) { instance_spy('watir element') }
        let(:watir) { class_spy('watir') }

        before do
          stub_const('Watir', watir)
          allow(browser).to receive(:wait_until).and_yield(watir_element)
        end

        context 'when calling #element' do
          it 'waits until the element exists by default', :aggregate_failures do
            component_instance.test_div

            expect(browser).to have_received(:wait_until)
            expect(watir_element).to have_received(:exists?)
          end

          it 'changes the default_timeout to the wait specified then back' do
            component_instance.test_div(wait: 0)

            expect(watir).to have_received(:default_timeout=).with(0)
            expect(watir).to have_received(:default_timeout=).with(Chemlab.configuration.default_timeout)
          end
        end

        context 'when calling #element?' do
          it 'does not wait for the element to exist by default' do
            component_instance.test_div?

            expect(watir_element).not_to have_received(:exists?)
          end

          it 'allows overrides' do
            component_instance.test_div?(wait: 10, wait_until: :exists?)

            expect(watir_element).to have_received(:exists?)
            expect(watir).to have_received(:default_timeout=).with(10)
          end
        end

        context 'when calling #element_element' do
          it 'has no wait by default' do
            component_instance.test_div_element

            expect(watir).not_to have_received(:default_timeout=)
            expect(browser).not_to have_received(:wait_until)
          end
        end

        context 'when defining elements with defaults' do
          describe ':test_div' do
            it 'has a wait of 20 seconds' do
              component_with_custom_args.new.test_div

              expect(watir).to have_received(:default_timeout=).with(20)
            end
          end

          describe ':test_text_field' do
            it 'checks both exists? and enabled? conditions' do
              component_with_custom_args.new.test_text_field

              expect(watir_element).to have_received(:exists?)
              expect(watir_element).to have_received(:enabled?)
            end
          end

          describe ':test_button' do
            it 'checks is :clickable?' do
              component_with_custom_args.new.test_button

              expect(watir_element).to have_received(:present?)
            end
          end
        end
      end
    end
  end
end
