# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  RSpec.describe Library do
    context 'when employing pages from multiple libraries' do
      let!(:library1_module) do
        module ::LibraryOne
          include Chemlab::Library

          self.base_url = 'https://www.library.one'

          class Home < Chemlab::Page
            path '/home'
          end
        end
      end

      let!(:library2_module) do
        module ::LibraryTwo
          include Chemlab::Library

          self.base_url = 'https://www.library.two'

          class Home < Chemlab::Page
            path '/home-two'
          end
        end
      end

      let(:watir) { instance_spy('watir') }

      before do
        stub_const('Watir::Browser', watir)

        Chemlab.configure do |chemlab|
          chemlab.browser = :chrome
          chemlab.base_url = 'https://library.none'
        end

        LibraryOne::Home.perform(&:visit)
        LibraryTwo::Home.perform(&:visit)
      end

      it 'resolve to the correct url' do
        aggregate_failures do
          expect(watir).to have_received(:goto).with('https://www.library.one/home').ordered
          expect(watir).to have_received(:goto).with('https://www.library.two/home-two').ordered
        end
      end
    end

    describe 'accessors' do
      let!(:mod1) do
        module Mod1
          include Library
        end
      end

      let!(:mod2) do
        module Mod2
          class << self
            attr_accessor :base_url
          end
        end
      end

      before do
        Mod1.base_url = Mod2.base_url = 'https://example.com'
      end

      it 'defines a getter' do
        aggregate_failures do
          expect(Mod1.base_url).to eq('https://example.com')
          expect(Mod2.base_url).to eq('https://example.com')
        end
      end

      it 'defines a setter' do
        expect(Mod1).to respond_to(:base_url=)
        expect(Mod2).to respond_to(:base_url=)
      end
    end

    context 'when Chemlab is configured with a base_url' do
      let!(:mod) do
        module Mod
          include Library
        end
      end

      before do
        Chemlab.configure do |chemlab|
          chemlab.base_url = 'https://test.example.com'
        end
      end

      it 'defaults to the Chemlab base_url' do
        expect(Mod.base_url).to eq('https://test.example.com')
      end
    end
  end
end
