# frozen_string_literal: true

require_relative 'integration/libraries'

Chemlab.configure do |config|
  config.browser = :chrome, { headless: true }
  config.base_url = "file://#{File.expand_path('./integration/vendor', __dir__)}"
end
