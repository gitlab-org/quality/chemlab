# frozen_string_literal: true

require 'integration_helper'

module Chemlaboratory
  RSpec.describe 'Flows' do
    describe 'SignIn Flow' do
      let(:default_username) { 'default_user' }
      let(:default_password) { 'default_pass' }
      let(:test_username) { 'test_user' }
      let(:test_password) { 'test_pass' }

      it 'signs in with default credentials' do
        Flow::SignIn.sign_in

        Page::SignIn.perform do |sign_in|
          aggregate_failures do
            expect(sign_in.username).to eq(default_username)
            expect(sign_in.password).to eq(default_password)
            expect(sign_in).to be_signed_in
          end
        end
      end

      it 'accepts parameters and signs in' do
        Flow::SignIn.sign_in(username: test_username, password: test_password)

        Page::SignIn.perform do |sign_in|
          aggregate_failures do
            expect(sign_in.username).to eq(test_username)
            expect(sign_in.password).to eq(test_password)
            expect(sign_in).to be_signed_in
          end
        end
      end
    end
  end
end
