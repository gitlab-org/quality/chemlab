# frozen_string_literal: true

module Chemlaboratory
  module Flow
    module SignIn
      module_function

      # Sign in as a particular user
      # @note Defaults to default_user/default_pass if parameters not specified
      # @example
      #   Flow::SignIn.sign_in #=> sign in as default_user/default_pass
      #   Flow::SignIn.sign_in(username: 'my_username', password: 'my_password')
      # @see Page::SignIn
      def sign_in(username: 'default_user', password: 'default_pass')
        Page::SignIn.perform do |sign_in|
          sign_in.visit unless sign_in.on_page?

          sign_in.username = username
          sign_in.password = password
          sign_in.sign_in
        end
      end
    end
  end
end
