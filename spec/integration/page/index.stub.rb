# frozen_string_literal: true

module Chemlaboratory
  module Page
    module Index
      # @note Defined as +h1 :header_title+
      # @return [String] The text content or value of +header_title+
      def header_title
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::Index.perform do |index|
      #     expect(index.header_title_element).to exist
      #   end
      # @return [Watir::H1] The raw +H1+ element
      def header_title_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::Index.perform do |index|
      #     expect(index).to be_header_title
      #   end
      # @return [Boolean] true if the +header_title+ element is present on the page
      def header_title?
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @note Defined as +link :sign_in+
      # Clicks +sign_in+
      def sign_in
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::Index.perform do |index|
      #     expect(index.sign_in_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def sign_in_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::Index.perform do |index|
      #     expect(index).to be_sign_in
      #   end
      # @return [Boolean] true if the +sign_in+ element is present on the page
      def sign_in?
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @note Defined as +link :show_dynamic_element+
      # Clicks +show_dynamic_element+
      def show_dynamic_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::Index.perform do |index|
      #     expect(index.show_dynamic_element_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def show_dynamic_element_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::Index.perform do |index|
      #     expect(index).to be_show_dynamic_element
      #   end
      # @return [Boolean] true if the +show_dynamic_element+ element is present on the page
      def show_dynamic_element?
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @note Defined as +div :hidden_element+
      # @return [String] The text content or value of +hidden_element+
      def hidden_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::Index.perform do |index|
      #     expect(index.hidden_element_element).to exist
      #   end
      # @return [Watir::Div] The raw +Div+ element
      def hidden_element_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::Index.perform do |index|
      #     expect(index).to be_hidden_element
      #   end
      # @return [Boolean] true if the +hidden_element+ element is present on the page
      def hidden_element?
        # This is a stub, used for indexing. The method is dynamically generated.
      end
    end
  end
end
