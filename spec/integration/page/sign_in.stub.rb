# frozen_string_literal: true

module Chemlaboratory
  module Page
    module SignIn
      # @note Defined as +text_field :username+
      # @return [String] The text content or value of +username+
      def username
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # Set the value of username
      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     sign_in.username = 'value'
      #   end
      # @param value [String] The value to set.
      def username=(value)
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     expect(sign_in.username_element).to exist
      #   end
      # @return [Watir::TextField] The raw +TextField+ element
      def username_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     expect(sign_in).to be_username
      #   end
      # @return [Boolean] true if the +username+ element is present on the page
      def username?
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @note Defined as +text_field :password+
      # @return [String] The text content or value of +password+
      def password
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # Set the value of password
      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     sign_in.password = 'value'
      #   end
      # @param value [String] The value to set.
      def password=(value)
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     expect(sign_in.password_element).to exist
      #   end
      # @return [Watir::TextField] The raw +TextField+ element
      def password_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     expect(sign_in).to be_password
      #   end
      # @return [Boolean] true if the +password+ element is present on the page
      def password?
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @note Defined as +button :sign_in+
      # Clicks +sign_in+
      def sign_in
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     expect(sign_in.sign_in_element).to exist
      #   end
      # @return [Watir::Button] The raw +Button+ element
      def sign_in_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     expect(sign_in).to be_sign_in
      #   end
      # @return [Boolean] true if the +sign_in+ element is present on the page
      def sign_in?
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @note Defined as +span :signed_in+
      # @return [String] The text content or value of +signed_in+
      def signed_in
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     expect(sign_in.signed_in_element).to exist
      #   end
      # @return [Watir::Span] The raw +Span+ element
      def signed_in_element
        # This is a stub, used for indexing. The method is dynamically generated.
      end

      # @example
      #   Chemlaboratory::Page::SignIn.perform do |sign_in|
      #     expect(sign_in).to be_signed_in
      #   end
      # @return [Boolean] true if the +signed_in+ element is present on the page
      def signed_in?
        # This is a stub, used for indexing. The method is dynamically generated.
      end
    end
  end
end
