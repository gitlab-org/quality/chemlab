# frozen_string_literal: true

require_relative '../lib/chemlab'
require 'climate_control'
require 'rspec-parameterized'

RSpec.configure do |rspec|
  rspec.disable_monkey_patching!
  rspec.order = :random
  rspec.mock_with :rspec do |mock|
    mock.allow_message_expectations_on_nil = true
  end

  rspec.define_derived_metadata(file_path: %r{spec/unit/}) do |metadata|
    metadata[:unit] = true
  end
end
