# @title Develop

## Setting up Chemlab

Check out the source from GitLab.

```bash
$ git clone https://gitlab.com/gitlab-org/quality/chemlab.git
```

Ensure you have the correct version of Ruby.

```bash
# using RVM
$ rvm install $(cat .ruby-version) && rvm use $(cat .ruby-version)
```

Ensure bundler is installed and run.

```bash
$ gem install bundler:2.1.4
$ bundle install
```

## Testing

There are two different layers of testing internally in Chemlab.

1. **[Unit Tests](#unit-tests)** Tests that assert the low-level functionality within Chemlab. 
1. **[Integration Tests](#integration-tests)** A set of tests that use Chemlab as the engine to test that the framework
   works. These tests serve also as examples for prospective users of Chemlab.

> **Note** All tests reside under `spec/` and _must be run exclusively_.  Running a `$ bundle exec rspec` with no arguments
> passed to RSpec will likely result in a failure due to internal test-bed conflicts.

### Unit Tests

```shell
$ bundle exec rspec spec/unit
```

### Integration Tests

```shell
$ bundle exec rspec spec/integration
```

All integration tests by default run in headless mode. If you'd like to see the tests run, change the line in `spec/integration_helper.rb`:

```ruby
# change ...
chemlab.browser = :chrome, { headless: true }

# to ...
chemlab.browser = :chrome
```
