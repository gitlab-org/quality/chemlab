# @title Getting Started with Chemlab

This guide will help you get started with using Chemlab.

> See [Writing Libraries](writing_libraries.md) if you are _writing_ a [library](design_scaffold.md#libraries).

## Installing

### Add Chemlab to your project

**Gemfile**
```ruby
gem 'chemlab'
```

### Configure Chemlab

In your UI `spec_helper.rb` file:

```ruby
Chemlab.configure do |chemlab|
  chemlab.base_url = 'https://example.com' # no trailing slash
  chemlab.browser = :chrome
  chemlab.libraries = []
end
```

### Configuring Chemlab with an existing WebDriver instance

In your UI `spec_helper.rb` file:

```ruby
# If Capybara is already being used:
Chemlab.configure do |chemlab|
  chemlab.base_url = 'https://example.com' # no trailing slash
  chemlab.browser = Capybara.current_session.driver.browser # reuse Capybara session
  chemlab.libraries = []
end
```

### [`component/`](components.md)

### [`page/`](pages.md)

### [`flow/`](flows.md)

### `spec/`

> All tests should reside within this directory

#### `spec/api`

> API-specific tests.  These tests will not touch the UI.

#### `spec/unit`

> Specs written to test local framework-specific functionality.

#### `spec/spec_helper.rb`

> spec_helper.rb should be included within all specs written, sans [`spec/unit/`](#spec-unit)
> This file should also configure Chemlab with the desired browser, Application Under Test (AUT) base URL.
> This file would also be used for configuring RSpec.

```ruby
require 'chemlab'

Chemlab.configure do |chemlab|
  chemlab.browser = :chrome, { headless: true }
  chemlab.base_url = 'http://example.com'

  chemlab.configure_rspec do |rspec|
    # configure RSpec here (optional)
  end
end
```

# `qa/qa.rb`

> Module used to autoload all page and component libraries

```ruby
module QA
  module Page
    autoload :Login, 'page/login'
  
    module Users
      autoload :Index, 'page/users/index'
    end
  end
  
  module Component
    autoload :Navbar, 'component/navbar'
  end
end
```
