# @title Configuration

| Configuration                 | Required | Default |
| -------------                 | -------- | ------- |
| [`base_url`](#base_url)       | Yes      |         |
| [`hide_banner`](#hide_banner) | No       | `true`  |
| [`libraries`](#libraries)     | No       | `[]`    |

## `base_url`

The URL that will be used as the base for all relative pathing.

```ruby
Chemlab.configure do |chemlab|
  chemlab.base_url = 'https://example.com'
end
```

## `hide_banner`

Chemlab comes with a banner that outputs to the configured output stream

```ruby
Chemlab.configure do |chemlab|
  chemlab.hide_banner = true
end
```

## `libraries`

The libraries that will be exposed to the `Chemlab::Vendor` module within tests.

Given that we are using [The Internet](writing_libraries.md#6-using-your-library) as the example library:

```ruby
Chemlab.configure do |chemlab|
  chemlab.libraries = [TheInternet]
end
```
