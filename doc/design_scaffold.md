# @title Design Scaffold

## Project Structure

```ruby
my_project/
  lib/                    #=> The source of the application
  spec/                   #=> Specs for the application
  qa/                     
    component/            #=> All Component libraries
    flow/                 #=> All Flows
    layer/                #=> All Layer libraries
    modal/                #=> All Modal libraries
    page/                 #=> All Page libraries
    spec/api/             #=> API-based tests
      api_spec.rb
      spec_helper.rb      #=> spec_helper that will configure the underlying test runner for running API tests
    spec/unit/            #=> All unit tests to test QA-specific code, including Page Library helper methods
      unit_spec.rb
      spec_helper.rb      #=> spec_helper that will configure the underlying test runner for running unit tests for the QA libraries
    spec/ui/              #=> All UI end-to-end tests
      end_to_end_spec.rb  
      spec_helper.rb      #=> spec_helper that will configure the underlying test runner for running UI end-to-end tests
```

> **Note** This structure is not required but it is recommended.

## Design and Architecture

![design architecture](img/design_architecture_v1.png)

## Libraries

Each collection of Page Libraries is held within its respective project, usually as a Rubygem.

When a project would like to reference another project, Chemlab can import that library in for use,
much like how a "vendor" directory would function. 

### Adding a library as a Gem

> In this example we'll be adding the GitLab Handbook Library to our project.

In your `Gemfile`:

```ruby
gem 'chemlab-library-www-gitlab-com'
```

Add the exposed Ruby module to Chemlab's Library definition:

```ruby
Chemlab.configure do |chemlab|
  chemlab.libraries = [GitlabHandbook]
end
```

The library will now be accessible through the `Chemlab::Vendor` module:

```ruby
it 'works with the AUT in addition to the handbook' do
  # some of your framework-specific code goes here
  
  Chemlab::Vendor::GitlabHandbook::Page::About.perform(&:get_free_trial)
end
```

### Adding a library using the CLI

```shell
# TODO add a library from a Git repository
$ chemlab add https://gitlab.com/gitlab-org/quality/chemlab-library-www-gitlab-com.git

# TODO: add a library from the registry 
$ chemlab add www-gitlab-com

# TODO add .chemlab.yml configuration file
```

## Definitions and Examples

### Components

Components are the basis of any UI representation, be that a Page, a Pop-up window, a Layer, etc.

### Flow

### Layers

A layer is a User Interface component similar to a Modal in that it presents information to the user as a child window,
the only difference between that it does not _disable_ the main window.

```ruby
# modal.rb

# Base class for Modals that will implement accepting and cancelling
class Layer < Chemlab::Component
  button :close
  div :content
end
```

```ruby
# spec/ui/modal_spec.rb
it 'shows the alert' do
  Layer.perform do |layer|
    expect(layer).to be_visible
    expect(layer.content).to include('Some informative message.')
  end
end
```

### Modals

[From Wikipedia](https://en.wikipedia.org/wiki/Modal_window)

> A modal window creates a mode that disables the main window but keeps it visible, with the modal window as a child window in front of it.

```ruby
# modal.rb

# Base class for Modals that will implement accepting and cancelling
class Modal < Chemlab::Component
  button :confirm
  button :cancel
  h4 :title
  div :content
end
```

```ruby
# spec/ui/modal_spec.rb
it 'shows the alert' do
  Modal.perform do |alert|
    expect(alert).to be_visible
    expect(alert.title).to eq('Alert!')
    expect(alert.content).to include('Alert content')
  end
end
```

### Pages

Generate a Login page, with the `path` attribute set to `/users/sign_in`:

```shell
$ chemlab generate page Login path=/users/sign_in

class Login
  path '/users/sign_in'
end
```

Given the [layout above](#project-structure), you can also do:

```shell
$ chemlab generate page Login path=/users/sign_in > qa/page/login.rb
```

#### Helper Methods

Helper methods are methods dedicated to "helping" the engineer achieve bulk tasks.

Given an example Login Page Library:

```ruby
class Login < Chemlab::Page
  text_field :username
  text_field :password
  button :sign_in
  
  # Signs in the user with a given username and password
  # @param [String] username to sign in with
  # @param [String] password to sign in with
  # @return [void]
  # @example
  #   Login.perform { |login| login.login('user', 'pass') }
  def login(username, password)
    self.username = username
    self.password = password
    sign_in
  end
end
```

The helper method `login` is used to perform several tasks.

1. Set the username
1. Set the password
1. Click the sign in button

### Other

Since [Modals](#modals) and [Layers](#layers) are simply an additional way to present information to somebody,
creating another UI component is very simple and is _encouraged_.

<!-- TODO: Link Chemlab::Component to API docs -->
Extend the `Chemlab::Component` class with your base class.

## Rails Plug-in

TODO: When running a `rails g controller ThePageController`, run `chemlab generate page ThePage`
