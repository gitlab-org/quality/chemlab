# Waiting

## Configuring Chemlab's default wait

By default, Chemlab will forward `Chemlab.configuration.default_timeout` to `Watir.default_timeout`, meaning that when
unchanged, `Chemlab.configuration.default_timeout` will return `Watir.default_timeout` which returns `30` seconds.

To override this, you can change this during Chemlab's configuration block:

```ruby
Chemlab.configure do |chemlab|
  chemlab.default_timeout = 10 # 10 seconds
end

# or

Chemlab.configuration.default_timeout = 10 # 10 seconds
```

## An element that does not appear immediately

You may override the default wait for the element in question.

```ruby
# wait for a max of 60 seconds for the text field to exist
text_field :username, id: 'username', wait: 60
```

If you do not want to override the element's wait and would rather execute the wait at runtime:

```ruby
  class MyPage < Chemlab::Page
    button :my_button
  end

  MyPage.perform do |page|
    page.my_button if page.my_button? # click +my_button+ if my_button exists right now (i.e., doesn't wait)
    
    page.my_button if page.my_button?(wait: 30) # wait at max 30 seconds for my_button to appear.  Once it appears, click it
    
    page.my_button(wait: 5, wait_until: :present?) # wait for 5 seconds until the button is clickable
    
    page.my_button(wait: 10) # attempt to find the button in 10 seconds, then click the button
  end
```

## Specifying default element waits

```ruby
class MyPage < Chemlab::Page
  # wait explicitly for the default +Chemlab.configuration.default_timeout+ until the element exists
  h1 :header
  
  # wait explicitly for a max of 10 seconds until the element exists
  h1 :header, id: 'header', wait: 10
  
  # wait until the field is enabled for a max time of 0 seconds
  text_field :field, id: 'field', wait: 0, wait_until: :enabled?
  
  # wait for the field to exist and to be enabled for the default +Chemlab.configuration on both conditions
  text_field :field, id: 'field', wait_until: %i[exist? enabled?]
end
```
