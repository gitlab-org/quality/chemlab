# @title Writing Libraries

## Generating the project scaffold

If Chemlab is not yet installed, install it using `gem install chemlab`. 

```shell
$ cd ~/projects
$ chemlab new the-internet
```

A new directory should now appear in `~/projects/the-internet` with the project structure:

```
the-internet/
  lib/
    page/
      sample.rb
    the_internet.rb
  spec/
    unit/
      page/sample_spec.rb
    integration/
      page/sample_spec.rb
  .gitignore
  the-internet.gemspec
  Gemfile
  README.md
```

### Modifying the skeleton

After the project is scaffolded, you will need to then change each file to tailor it to your project.

1. Edit the `Gemfile` to include your own Rubygems.
1. Edit `my-library.gemspec` to include information specific to your project.

## Write the Page Libraries

In this example, we will be writing page libraries for `https://the-internet.herokuapp.com/` which is a popular
website for writing automated tests for, since it accounts for a lot of different actions that may come up during automation.

### Index Page

#### 1. Generate the page

Generate a page called `Index` and put it in the `page` directory, and specify the root `/` as the path:

```shell
$ chemlab generate page Index path=/ > lib/page/index.rb
```

### 2. Tailor the code

#### 2.a Wrap the class in your Module

Edit `lib/page/index.rb` to be more Project-specific and wrap it in the global Module to be exposed.

```ruby
# frozen_string_literal: true

module TheInternet
  class Index < Chemlab::Page
    path '/'
  end
end
```

#### 2.b Replace `Sample` with `Index`

The `lib/page/sample.rb` file is merely there to provide a sample [page library](pages.md) and will not be needed.

1. Delete `sample.rb`.
2. Edit `lib/the_internet.rb` to replace `Sample` with `Index`:

```ruby
# frozen_string_literal: true

module TheInternet
  module Page
    autoload :Index, 'page/index'
  end
end
```

3. Refactor and replace `spec/integration/page/sample_spec.rb` to `index_spec.rb`

### 3. Adding elements

#### 3.a. Write the page library

It's a good idea to be detailed, even if you do not intend to use all of the elements.

- Open https://the-internet.herokuapp.com/ in your web-browser, and inspect each link in the list
- Add all of these links as elements to the `Index` class:

```ruby
# frozen_string_literal: true

module TheInternet
  module Page
    class Index < Chemlab::Page
      link :ab_test, href: '/abtest'
      link :add_remove_elements, href: '/add_remove_elements'
      link :basic_auth, href: '/basic_auth'
      
      # there are many more on this page, but for this example's sake, we will omit them.
    end
  end
end
```

#### 3.b. Generate stubs for the library

```shell
$ chemlab generate:stubs lib/page
```

### 4. Write the spec(s)

- Edit `spec/integration/page/index_spec.rb`:

```ruby
# frozen_string_literal: true

module TheInternet
  RSpec.describe 'Index' do
    before do
      Page::Index.perform(&:visit)
    end

    it 'should be that all elements are there' do
      Page::Index.perform do |index|
        expect(index).to be_visible
      end
    end
  end
end
```

### 5. Publish your Library to Rubygems

```shell
$ chemlab release
```

The rake task `release` will be performed which is inherited from `bundler/gem_tasks` which builds the gem, pushes a tag to Git and publishes the Gem to Rubygems.org.
The Rubygem should be published with the name `chemlab-library-the-internet` meaning it can now be used by Chemlab.

### 6. Using your Library

1. Edit your Gemfile

```ruby
# Gemfile

gem 'chemlab-library-the-internet'
```

2. [Configure Chemlab](configuration.md) to expose the new library

```ruby
require 'the_internet'

Chemlab.configure do |chemlab|
  chemlab.libraries = [TheInternet]
end
```

> **Note** The Chemlab Library should be prefixed with `chemlab-library-` 

### Defining a Library's base_url using the `Chemlab::Library` mixin

```ruby
# frozen_string_literal: true

module TheInternet
  include Chemlab::Library

  # `Chemlab::Library` exposes a base_url accessor on the Library module
  self.base_url = 'https://the-internet.herokuapp.com'

  class Index < Chemlab::Page
    path '/'
  end
end
```
