# @title Pages

A Page is a representation of a complete page within the UI.

Given the following HTML:

```html
<!DOCTYPE html>
<html>
<body>
  <form id="login">
    <input type="text" data-testid="username" name="username" />
    <input type="password" data-testid="password" name="password" />
    <input type="button" value="Log in" data-testid="login" />
  </form>
</body>
</html>
```

A Page Library representation of this in Chemlab would look like:

```ruby
module Page
  class Login < Chemlab::Page
    path '/users/sign_in'

    text_field :username
    text_field :password
    button :login
  
    def sign_in_as(username, password)
      self.username = username
      self.password = password
   
      login
    end
  end 
end
``` 

## Attributes

### `path`

> The path segment of the URL of the page, with a preceding slash (`/`)

Given these URLs:

```ruby
# https://example.com/users/sign_in
class Login < Chemlab::Page
  path '/users/sign_in'
end

# https://example.com/resources/new
class New < Chemlab::Page
  path '/resources/new'
end
```

## Methods

### `visit`

> Navigate to the page by forcing a URL redirect to the Page's [`path` attribute](#path)

## Descendent elements

> **TODO**: This is not yet implemented.

By passing a block to an element, you can find elements within other elements.

This is especially useful for smaller components, that may not need to be extracted into a separate `Component` subclass.

```ruby
module Page
  class Login < Chemlab::Page
    path '/users/sign_in'
    
    form :login_form, id: 'login' do
      text_field :username, name: 'username'
      text_field :password, name: 'password'
    end
  end
end
```

Will be the equivalent of the following raw Watir commands:

```ruby
form(id: 'login').text_field(name: 'username')
form(id: 'login').text_field(name: 'password')
```
